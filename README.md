# Setup for UI Katas

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

A JS/TS/Node setup for implementing UI kata with different popular frameworks.

## Configured frameworks frameworks

- React 16 and @testing-library/react
- Vue.js 2.6 and @testing-library/vue
- Angular 10 and @testing-library/angular

## Example kata

- [React TDD kata](./react_tdd_kata.md)
