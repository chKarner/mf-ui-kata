module.exports = {
  projects: [
    {
      displayName: '📚 Lib',
      testMatch: ['<rootDir>/src/lib/**/*.spec.js'],
    },
    {
      displayName: '⚛️ React',
      testMatch: ['<rootDir>/src/react/**/*.spec.js'],
      setupFilesAfterEnv: ['./jest.setup.js'],
    },
    {
      displayName: '🔭 Vue.js',
      testMatch: ['<rootDir>/src/vue/**/*.spec.js'],
      setupFilesAfterEnv: ['./jest.setup.js'],
    },
    {
      displayName: '🎣 Angular',
      testMatch: ['<rootDir>/src/angular/**/*.spec.ts'],
      preset: 'jest-preset-angular',
      setupFilesAfterEnv: ['./jest-angular.setup.ts'],
    },
  ],
};
