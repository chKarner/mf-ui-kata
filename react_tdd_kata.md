- You're building an issue tracking system
- Build a component which displays the names of assignees
  - eg. `<Assignees assignees={['Mike', 'Sepp', 'David']} />`
  - build a simple ui
- WITH more than 3 assigness,
  - only display 3 assignees
  - display a show more button
- WHEN the show more button was clicked
  - display all assignees
  - a show less button is displayed instead of a show more button
  - AND the show less button was clicked, only displays 3 assignees
- WITH less than 4 assignees,
  - don't display a "show more" button

---

by [@webPapaya](https://twitter.com/webpapaya) ([gist](https://gist.github.com/webpapaya/4b60c44eb23f5d7bff913707ca8abbfa))
