import { Component } from '@angular/core';
import { render, screen } from '@testing-library/angular';

@Component({
  selector: 'assignees',
  template: `Implement me!`,
})
export class AssigneesComponent {}

describe('Assignees', () => {
  test('renders the Angular Assignees component', () => {
    render(AssigneesComponent);

    expect(screen.getByText('Implement me!')).toBeInTheDocument();
  });
});
