function assignees() {
  return 'Implement me!';
}

describe('Assignees', () => {
  test('returns the correct value', () => {
    const result = assignees();

    expect(result).toEqual('Implement me!');
  });
});
