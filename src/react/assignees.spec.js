import React from 'react';
import { render, screen } from '@testing-library/react';

const Assignees = () => <span>Implement me!</span>;

describe('Assignees', () => {
  test('renders the React Assignees component', () => {
    render(<Assignees />);

    expect(screen.getByText('Implement me!')).toBeInTheDocument();
  });
});
