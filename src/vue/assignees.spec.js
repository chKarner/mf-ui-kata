import { render, screen } from '@testing-library/vue';

const Assignees = {
  template: `<span>Implement me!</span>`,
};

describe('Assignees', () => {
  test('renders the Vue.js Assignees component', () => {
    render(Assignees);

    expect(screen.getByText('Implement me!')).toBeInTheDocument();
  });
});
